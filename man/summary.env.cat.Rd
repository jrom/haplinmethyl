% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/summary.env.data.R
\name{summary.env.cat}
\alias{summary.env.cat}
\title{Info about the \code{env.cat} class}
\usage{
\method{summary}{env.cat}(object, ...)
}
\arguments{
\item{object}{The \code{env.cat} object read in by \link{envDataRead}.}

\item{...}{ignored}
}
\value{
list with:
  \itemize{
    \item class - the full class name;
    \item nrow - number of rows;
    \item ncol - number of columns;
    \item rownames - character vector with row names;
    \item colnames - character vector with column names.
  }
}
\description{
Prints a short summary about the categorical environmental data.
}
